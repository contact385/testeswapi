import axios from 'axios'

const modelApi = axios.create({
  baseURL: process.env.VUE_APP_MODEL_API_URL,
  timeout: 60000,
})

const swapi = axios.create({
  baseURL: process.env.VUE_APP_MODEL_SWAPI_URL,
  timeout: 60000,
})

modelApi.interceptors.request.use()
swapi.interceptors.request.use()

export {
  modelApi,
  swapi
}
