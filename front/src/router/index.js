import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Lista from '@/components/Lista'
import Config from '@/components/Config'
import Login from '@/components/Login'


import store from '@/store'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/lista',
    name: 'Lista',
    component: Lista
  },
  {
    path: '/config',
    name: 'Configuração',
    component: Config
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

const router = new Router({ mode: 'history', routes })

router.beforeEach(async (to, from, next) => {
  let auth = store.getters['swapi/auth']

  if (!auth) {
    const user = localStorage.getItem('user') ? localStorage.getItem('user') : null
    const pass = localStorage.getItem('pass') ? localStorage.getItem('pass') : null

    if (user && pass) {
      auth = true
    }
  }

  if (!auth && from.path != '/Login' && to.path != '/login') {
    router.push({ name: 'Login' })
    return
  }

  next()
})

export default router
