import { modelApi, swapi }  from '@/api'


const load = async context => {
  try {
    const check = await modelApi.get('/check')
    context.commit('SAVE_DATA', check.data)
    return true
  }catch(err){
    console.log(err, 'err')
  }
}

const search = async (context, data) => {
  try {
    const check = await swapi.get(data)
    return check.data
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const saveplanet = async (context, data) => {
  try {
    const check = await modelApi.post('/saveplanet', data)
    if(check) return true
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const savepeople = async (context, data) => {
  try {
    const check = await modelApi.post('/savepeople', data)
    if(check) return true
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const savefilm = async (context, data) => {
  try {
    const check = await modelApi.post('/savefilm', data)
    if(check) return true
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const savestarship = async (context, data) => {
  try {
    const check = await modelApi.post('/savestarship', data)
    if(check) return true
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const savespecie = async (context, data) => {
  try {
    const check = await modelApi.post('/savespecie', data)
    if(check) return true
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const savevehicles = async (context, data) => {
  try {
    const check = await modelApi.post('/savevehicles', data)
    if(check) return true
  }catch(err){
    return false
    console.log(err, 'err')
  }
}

const login = async (context, data) => {
  if(data.user == "sury" && data.pass == 123){
    localStorage.setItem('user', data.user);
    localStorage.setItem('pass', data.pass);
    return true
  }else{
    return false
  }
}


export default {
  login,
  fetch,
  load,
  search,
  saveplanet,
  savevehicles,
  savespecie,
  savestarship,
  savefilm,
  savepeople
}
