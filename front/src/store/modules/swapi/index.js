import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import consts from './consts'

const state = {
  data: {
    "films":[],
    "people":[],
    "planets":[],
    "species":[],
    "starships":[],
    "vehicles":[] 
  },
  auth:null
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
}
