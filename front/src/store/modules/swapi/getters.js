const data = state => state.data
const auth = state => state.auth

export default {
  data,
  auth
}
