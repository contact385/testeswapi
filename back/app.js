const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const port = 3000

var fs = require('fs');
var cors = require('cors');
const { send } = require('process');
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.get('/check', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);
    res.json(resultArray)
  });

})

app.post('/saveplanet', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);

    if(req.body.count){
      console.log(req.body.results)
      const result = req.body.results
      for (let index = 0; index < result.length; index++) {
        const iten = resultArray.planets.find(c => c.name == result[index].name)
        if(!iten){
          resultArray.planets.push(result[index])
          let data = JSON.stringify(resultArray);
          fs.writeFileSync('data/SWDB.json', data);
        }
      }
    }else{
      const iten = resultArray.planets.find(c => c.name == req.body.name)
      if(!iten){
        resultArray.planets.push(req.body)
        let data = JSON.stringify(resultArray);
        fs.writeFileSync('data/SWDB.json', data);
      }
    }
    res.json(resultArray)
  });
})

app.post('/savepeople', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);

    if(req.body.count){
      console.log(req.body.results)
      const result = req.body.results
      for (let index = 0; index < result.length; index++) {
        const iten = resultArray.people.find(c => c.name == result[index].name)
        if(!iten){
          resultArray.people.push(result[index])
          let data = JSON.stringify(resultArray);
          fs.writeFileSync('data/SWDB.json', data);
        }
      }
    }else{
      const iten = resultArray.people.find(c => c.name == req.body.name)
      if(!iten){
        resultArray.people.push(req.body)
        let data = JSON.stringify(resultArray);
        fs.writeFileSync('data/SWDB.json', data);
      }
    }
    res.json(resultArray)
  });
})

app.post('/savefilm', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);

    if(req.body.count){
      console.log(req.body.results)
      const result = req.body.results
      for (let index = 0; index < result.length; index++) {
        const iten = resultArray.films.find(c => c.title == result[index].title)
        if(!iten){
          resultArray.films.push(result[index])
          let data = JSON.stringify(resultArray);
          fs.writeFileSync('data/SWDB.json', data);
        }
      }
    }else{
      const iten = resultArray.films.find(c => c.title == req.body.title)
      if(!iten){
        resultArray.films.push(req.body)
        let data = JSON.stringify(resultArray);
        fs.writeFileSync('data/SWDB.json', data);
      }
    }
    res.json(resultArray)
  });
})

app.post('/savestarship', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);

    if(req.body.count){
      console.log(req.body.results)
      const result = req.body.results
      for (let index = 0; index < result.length; index++) {
        const iten = resultArray.starships.find(c => c.name == result[index].name)
        if(!iten){
          resultArray.starships.push(result[index])
          let data = JSON.stringify(resultArray);
          fs.writeFileSync('data/SWDB.json', data);
        }
      }
    }else{
      const iten = resultArray.starships.find(c => c.name == req.body.name)
      if(!iten){
        resultArray.starships.push(req.body)
        let data = JSON.stringify(resultArray);
        fs.writeFileSync('data/SWDB.json', data);
      }
    }
    res.json(resultArray)
  });
})

app.post('/savespecie', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);

    if(req.body.count){
      console.log(req.body.results)
      const result = req.body.results
      for (let index = 0; index < result.length; index++) {
        const iten = resultArray.species.find(c => c.name == result[index].name)
        if(!iten){
          resultArray.species.push(result[index])
          let data = JSON.stringify(resultArray);
          fs.writeFileSync('data/SWDB.json', data);
        }
      }
    }else{
      const iten = resultArray.species.find(c => c.name == req.body.name)
      if(!iten){
        resultArray.species.push(req.body)
        let data = JSON.stringify(resultArray);
        fs.writeFileSync('data/SWDB.json', data);
      }
    }
    res.json(resultArray)
  });
})

app.post('/savevehicles', (req, res) => {
  fs.readFile("data/SWDB.json", "utf8", function(err, data){
    if(err) throw err;
    var resultArray = JSON.parse(data);

    if(req.body.count){
      console.log(req.body.results)
      const result = req.body.results
      for (let index = 0; index < result.length; index++) {
        const iten = resultArray.vehicles.find(c => c.name == result[index].name)
        if(!iten){
          resultArray.vehicles.push(result[index])
          let data = JSON.stringify(resultArray);
          fs.writeFileSync('data/SWDB.json', data);
        }
      }
    }else{
      const iten = resultArray.vehicles.find(c => c.name == req.body.name)
      if(!iten){
        resultArray.vehicles.push(req.body)
        let data = JSON.stringify(resultArray);
        fs.writeFileSync('data/SWDB.json', data);
      }
    }
    res.json(resultArray)
  });
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})